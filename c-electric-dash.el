;;; This implements the electric dash (-) for C mode, for C++ mode and for Java
;;; mode.

;;; The idea is that in the context of a variable, a dash can be automatically
;;; converted to an underscore.  In the context of a 2 character operator, it
;;; should remain as a dash.

(if (not c-mode-map)
  (setq c-mode-map (make-sparse-keymap)))

(define-key c-mode-map "-" 'electric-c-dash)
(define-key c-mode-map "=" 'electric-c-dash-fixup)
(define-key c-mode-map ">" 'electric-c-dash-fixup)

(add-hook 'c++-mode-hook 'turn-on-electric-c++)
(add-hook 'java-mode-hook 'turn-on-electric-java)

(defun turn-on-electric-c++ ()
  "Turn on the electric - stuff when C++ mode is loaded."
  (if c++-mode-map
      (progn
	(define-key c++-mode-map "-" 'electric-c-dash)
	(define-key c++-mode-map "=" 'electric-c-dash-fixup)
	(define-key c++-mode-map ">" 'electric-c-dash-fixup))))
      

(defun turn-on-electric-java ()
  "Turn on the electric - stuff when Java mode is loaded."
  (if java-mode-map
      (progn
	(define-key java-mode-map "-" 'electric-c-dash)
	(define-key java-mode-map "=" 'electric-c-dash-fixup)
	(define-key java-mode-map ">" 'electric-c-dash-fixup))))
      

(defconst c-mode-electric-dash-to-underscore
  "[A-Za-z0-9]"
  "Regular expression for characters preceeding the dash that cause it to be
converted to an underscore.")

;;; If someone wants to change this variable, make the change only for that
;;; buffer and not all buffers.
(make-variable-buffer-local 'c-mode-electric-dash-to-underscore)

(defun electric-c-dash (arg)
  "Convert a dash to an underscore, depending upon its context."
  (interactive "P")
  (let ((what-to-insert ?-)
	(case-fold-search nil))
    (if (not (bobp))
	(progn
	  (backward-char)
	  (if (looking-at c-mode-electric-dash-to-underscore)
	      (progn
		(setq what-to-insert ?_)
		(forward-char))
	    ;;ELSE
	    (if (looking-at "_")
		(progn
		  ;; We had a dash that was prematurely converted to
		  ;; an underscore.  Delete it and put back the dash.
		  (delete-char 1)
		  (insert "-"))
	      ;;ELSE
	      (forward-char)))))
    (insert what-to-insert)))

(defun electric-c-dash-fixup (arg)
  "Check to see if we need to fixup a dash that was prematurely converted to an
underscore."
  (interactive "P")
  (backward-char)
  (if (looking-at "_")
      (progn
	(delete-char 1)
	(insert "-"))
    ;;ELSE
    (forward-char))
  (insert last-input-event))
