;;; -*-Mode:Lisp-Interaction -*-

;;; Insert a Java toString method as the last method of the class where the
;;; cursor is located.

(defvar foo nil)

(defun insert-tostring ()
  (interactive)
  ;;               1------------1 2------------2         3---------------3
  (if (looking-at "\\(public\\s-+\\)?\\(static\\s-+\\)class\\s-+\\([a-zA-Z0-9_]+\\)")
;;  (if (looking-at "\\(public\\s-+\\)?\\(static\\s-+\\)class\\s-+")
;;      (setq foo "found")
      (setq foo (buffer-substring-no-properties (match-beginning 3) (match-end 3)))
    ;;
    (setq foo "not found")))
