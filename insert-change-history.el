;;; -*- Mode: lisp-interaction -*-

;;;  Change history:
;;;  --------------------------------------------------------------------------
;;;  TWE   03/08/93	Change initials for Monique and Dave.
;;;  TWE   05/31/95	Fixed movement to end of the comment block for C++.


;;; This file defines a function which makes it easier to add a change history
;;; line to a C source file.

;;; It is assumed that the source file has a "Change history:" line which is
;;; enclosed by the C commenting characters.

(defvar add-to-top-p nil
  "Add the new change history line to the top (t) or bottom (nil) of the change history list.")

(defvar change-history-string "Change history:"
  "What to search for to find the change history.")

(defconst username-to-initials-list
  '(("tekberg" "TWE") ("whenry" "WH") (hdejohn"" "HDJ"))
  "List of username,initial pairs")
				      
(defun mmddyy (date-string-arg)
  "Return the date in mm/dd/yy format"
  (let* ((date-string (or date-string-arg (current-time-string)))
	 (month '(("Jan" . "01")("Feb" . "02")("Mar" . "03")
		  ("Apr" . "04")("May" . "05")("Jun" . "06")
		  ("Jul" . "07")("Aug" . "08")("Sep" . "09")
		  ("Oct" . "10")("Nov" . "11")("Dec" . "12")))
	 ;;  012345678901234567890123
	 ;; "Thu Nov  5 10:42:45 1992"
	 (month (cdr (assoc (substring date-string 4 7) month)))
	 (day   (substring date-string 8 10))
	 (year  (substring date-string 22)))
    (if (= (aref day 0) ? )
	(aset day 0 ?0))
    (concat month "/" day "/" year)))

    
(defun generate-initials ()
  "Return a string which are the initials of the current user."
  (let* ((username (get-process-environment "USER"))
	 (initial-element (assoc username username-to-initials-list)))
    (if initial-element
	(cadr initial-element)
      ;;ELSE
      (upcase (substring username 0 (min (length username) 3))))))


(defun insert-change-history ()
  (interactive)
  (let* ((comment-end-string (if (and (> (length comment-end) 0)
				      (= (aref comment-end 0) ? ))
				 (substring comment-end 1)
			       ;;ELSE
			       comment-end))
	 (comment-start-string (if (and (> (length comment-start) 1)
					(equal (substring comment-start 1 2)
					       " "))
				   (substring comment-start 0 1)
				 ;;ELSE
				 comment-start))
	 ;; True if there is no comment end string.
	 (comment-start-only (or (null comment-end-string)
				 (equal comment-end-string "")))
	 (global-comment (if (equal comment-start-string "// ")
			     ;; Only use this for C++ change history lines.
			     comment-start-string
			   ;;ELSE
			   ;; Make the change history lines stand out for
			   ;; things like shell scripts.
			   (concat comment-start-string
				   comment-start-string
				   comment-start-string))))
    (goto-char (point-min))
    (if (and (not (search-forward change-history-string nil t))
	     (y-or-n-p "Change history line not present.  Insert it "))
	(progn
	  (if comment-start-only
	      (progn
		;; Insert special stuff here.
		(insert global-comment)
		(insert "\n")
		(insert global-comment)
		(insert "  ")
		(insert change-history-string)
		(insert "\n")
		(insert global-comment)
		(insert "  --------------------------------------------------------------------------\n")
		(insert "\n")
		(insert "\n"))
	    ;;ELSE
	    (insert comment-start-string)
	    (insert "\n")
	    (insert "  ")
	    (insert change-history-string)
	    (insert "\n")
	    (insert "  -----------------------------------------------------------------------------\n")
	    (insert "  ")
	    (insert comment-end-string)
	    (insert "\n\n"))))

    (goto-char (point-min))
    (if (search-forward change-history-string nil t)
	(progn
	  (if comment-start-only
	      (if add-to-top-p
		  (forward-line 2)
		;;ELSE
		(let ((global-comment-pattern global-comment))
		  (if (= (aref global-comment (- (length global-comment) 1))
			 ? )
		      (progn
			;; Last character is a blank.  Change it to match on a
			;; blank or a tab.
			(setq global-comment-pattern
			      (concat (substring global-comment 0 -1)
				      "[ \t]"))))
		  (forward-line 1)
		  (beginning-of-line 1)
		  (while (looking-at global-comment-pattern)
		    (forward-line 1))))
	    ;;ELSE
	    (if add-to-top-p
		(forward-line 2)
	      ;;ELSE
	      (search-forward comment-end-string nil t)))
	  (beginning-of-line 1)
	  (open-line 1)
	  (if comment-start-only
	      (insert global-comment))
	  (insert "  ")
	  (insert (generate-initials))
	  (insert "   ")
	  (insert (mmddyy nil))
	  (insert "\t")))))

;;; Local Variables:
;;; eval: (defun byte-compile-this-file () (write-region (point-min) (point-max) buffer-file-name nil 't) (byte-compile-file buffer-file-name) nil)
;;; write-file-hooks: (byte-compile-this-file)
;;; End:
