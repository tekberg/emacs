;;; -*-Mode:Lisp-Interaction -*-

;;; This file defines the command `install-macro' which is similar to the Zmacs
;;; command of the same name.  The use of this function is to bind a keyboard
;;; macro to a specific keystroke.  This makes keyboard macros much more
;;; useful, especially when more than one is being used.

;;; The following variable definitions look a bit contorted because they allow
;;; for reasonable generality.  The gensym function only refers directly to
;;; three variables: gensym-variables, gensym-format and gensym-update-form.
;;; In this way, one could build up a more complex symbol using multiple
;;; variables and not have to update the code to accomplish it.  The
;;; gensym-format variable is the first argument to a format function and the
;;; gensym-variables is the list of variables whose values are to be arguments
;;; to the format function.  The gensym-update-form is evaluated to update the
;;; variables to contain new values.

(defvar gensym-counter 0)
(defvar gensym-variables '(list gensym-counter))
(defvar gensym-update-form '(setq gensym-counter (1+ gensym-counter)))
(defvar gensym-format "GENSYM-%d")

(defun gensym ()
  "Return a unique symbol"
  (prog1
      (make-symbol (apply 'format gensym-format (eval gensym-variables)))
    (eval gensym-update-form)))

;;; I couldn't find out where this was defined as a variable or a function so I
;;; defined it this way.  I also couldn't figure out if one can define an
;;; integer directly (so that it has an X in it, instead of just a pure
;;; number).
(defvar control-x-prefix-character (aref "\^X" 0)
  "The control-X character.")

(defun install-macro ()
  "Bind a keyboard macro to a keystroke."
  (interactive)
  (let ((prefix nil)			; First char of a multi-char keystroke
	keystroke			; Main character
	(symbol (gensym))		; Holds function definition
	keystroke-string)		; String equivalent of prefix+keystroke
    (message "Enter keystroke for keyboard macro: ")
    (setq keystroke (read-char))
    (if (or (= keystroke meta-prefix-char)
	    (= keystroke control-x-prefix-character))
	(progn
	  ;; This is an ESC or a control-X prefix character.  We need to read
	  ;; another character to complete the keystroke.
	  (message "Enter second keystroke for keyboard macro: ")
	  (setq prefix keystroke)
	  (setq keystroke (read-char))))
    ;; Bind the last keyboard macro to the function cell of a symbol.
    (fset symbol last-kbd-macro)
    ;; Create a string made up of the character(s) of the keystroke.
    (setq keystroke-string (make-string (if (null prefix) 1 2) keystroke))
    (if prefix
	(aset keystroke-string 0 prefix))
    ;; Bind the keystroke to the function.
    (global-set-key keystroke-string symbol)))
