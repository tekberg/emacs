;;; -*-Mode:Lisp-Interaction -*-

;;; This code is used to insert HTML code into a page that indicates when the
;;; page was modified last.

;;; If the insert-modified-date-search-string is not found, insert it
;;; before this string.
(defconst insert-modified-date-insert-before
  "</BODY>")

(defconst insert-modified-date-search-string
  "<!-- Insert modified date here -->"
  "This is a string that is in a file that has a modified date HTML string.")

(defconst insert-modified-date-prefix
  "Last modified "
  "This string is inserted immediately before the date.")

(defconst insert-modified-date-suffix
  "."
  "This string is inserted immediately after the date.")

(defun insert-modified-date ()
  "Insert HTML code for a file modified date."
  (save-excursion
    (goto-char (point-min))
    (if (search-forward insert-modified-date-search-string nil t)
	(progn
	  (forward-char 1)
	  (forward-line 1)
	  (forward-line 1)
	  (let ((date-start (point))
		date-end)
	    (forward-line 1)
	    (setq date-end (point))
	    (delete-region date-start date-end))
	  (insert 
	   insert-modified-date-prefix
	   (current-time-string)
	   insert-modified-date-suffix)
	  (insert "\n"))
      ;;ELSE
      (if (search-forward insert-modified-date-insert-before nil t)
	  (progn
	    (beginning-of-line)
	    (insert insert-modified-date-search-string "\n"
		    "<p>\n"
		    "<small>\n"
		    insert-modified-date-prefix
		    (current-time-string) 
		    insert-modified-date-suffix "\n"
		    "</small>\n"))
	;;ELSE
	(message "This does not appear to be an HTML file")))))
