;;; Pretty print a region of shell script code.

(defun shell-indent-region (&optional arg)
  "Indent a region of shell code so that the control structures and enclosed
code are indented properly.  ARG non-nil says to give the first line an
indentation of 8 characters.  By default the indentation of the first line is
used."
  (interactive "P")
  (let ((start (if (< (mark) (point)) (mark) (point)))
	(end   (if (> (mark) (point)) (mark) (point)))
	(command "shell-pretty-print"))
    (shell-command-on-region start end 
			     (if arg
				 command
			       (concat command " -NESTED"))
			     t)))
