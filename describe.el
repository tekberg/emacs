;;; -*-Mode:Lisp-Interaction -*-

;;; Name: describe
;;; Author: Tom Ekberg  ekberg@csc.ti.com
;;; Date: 2/6/89
;;; Version: 1.1
;;; Change from previous version:
;;;    Added code to check for macro and autoload objects.
;;; Description:
;;;    This file contains the code for a reasonable describe function.  The
;;;    author wrote it up himself by looking at the type functions available in
;;;    GNU Emacs.  This is a bit different from the Common Lisp describe
;;;    function in that it also describes details of objects like keymaps and
;;;    buffers.
;;; Known problem areas:
;;;    There is no way to detect a window configuration type.  Objects of this
;;;    type are treated as `unknown' objects.

(defun describe (object)
  "Print out a description of an object."
  (princ object)
  (princ " is ")
  (cond ((null object)
	 (princ "nil."))
	((integerp object)
	 (princ "an integer."))
	((bufferp object)
	 (princ (describe-buffer-internal object)))
	;; Must put this before lists and vectors
	((keymapp object)
	 (princ (describe-keymap-internal object)))
	((processp object)
	 (princ (describe-process-internal object)))
	((markerp object)
	 (print (describe-marker-internal object)))
	((symbolp object)
	 (princ (describe-symbol-internal object)))
	((windowp object)
	 (princ (describe-window-internal object)))
	((stringp object)
	 (princ (format "a string of length %d." (length object))))
	((describe-autoloadp object)
	 (princ (describe-autoload-internal object)))
	((describe-functionp object)
	 (princ (describe-function-internal object)))
	((describe-macrop object)
	 (princ (describe-macro-internal object)))
	((listp object)
	 (princ (format "a list of length %d." (length object))))
	((vectorp object)
	 (princ (format "a vector of length %d." (length object))))
	((byte-code-function-p object)
	 (princ "is a byte compiled function."))
	(t
	  (princ "an unknown object.")))
  (princ "\n")
  object)

(defun describe-buffer-internal (object)
  "Return a detailed description of a buffer OBJECT."
  (let ((result "a buffer object"))
    (if (eq object (current-buffer))
	(setq result (concat result " for the current buffer")))
    (setq result (concat result "."))
    (let ((old-buffer (current-buffer))
	  read-only
	  file-name)
      (set-buffer object)
      (setq result (concat result (format "\nBuffer %s been modified and contains %d characters."
					  (if (buffer-modified-p object)
					      "has"
					    "has not")
					  (buffer-size))))
      (setq read-only buffer-read-only)
      (setq file-name (buffer-file-name (current-buffer)))
      (set-buffer old-buffer)
      (if read-only
	  (setq result (concat result (format "\nBuffer is read only."))))
      (if file-name
	  (setq result (concat result (format "\nFile name for buffer is %s."
					      file-name)))))
    (let ((hook-count 0))
      (mapcar (lambda (x)
                (let* ((name (symbol-name (car x)))
                       (len (length name)))
                  (if (or (equal (substring name -4) "hook")
                          (equal (substring name -5) "hooks"))
                      (setq hook-count (+ 1 hook-count)))))
              (buffer-local-variables object))
      (if (> hook-count 0)
          (progn
            (setq result (concat result "\nHas hook" (if (= hook-count 1) "" "s") " "))
            (mapcar (lambda (x)
                      (let* ((name (symbol-name (car x)))
                             (len (length name)))
                        (if (or (equal (substring name -4) "hook")
                                (equal (substring name -5) "hooks"))
                            (setq result (concat result (format "%S" x))))))
                    (buffer-local-variables object)))))
    result))

(defun describe-marker-internal (object)
  "Return a detailed description of a marker object."
  (let ((result (concat (format "a marker object for the buffer %s."
				(buffer-name (marker-buffer object)))
			(format "\nMarker is at position %d."
				(marker-position object)))))
    result))

(defun describe-symbol-internal (object)
  "Return a detailed description of a symbol, complete with its
name, properties, function definition and value."
  (let ((result (format "a symbol with the name %s."
			(symbol-name object))))
    (setq result (concat result
			 (if (boundp object)
			     (format "\nThe value of the symbol is %s."
				     (symbol-value object))
			   ;;ELSE
			   "\nSymbol has no value.")))
    (if (symbol-plist object)
	(progn
	  (setq result (concat result "\nSymbol has the following properties:"))
	  (let ((properties (symbol-plist object))
		prop)
	    (while (not (null properties))
	      (setq prop (car properties))
	      (setq result (concat result (format "\n  %s - " (symbol-name prop))))
	      (cond ((eq prop 'variable-documentation)
		     (setq result (concat result (documentation-property
						  'obarray
						  'variable-documentation))))
		    (t
		     (setq result (concat result (format "%s" (car (cdr properties)))))))
	      (setq properties (cdr (cdr properties))))))
      ;;ELSE
      (setq result (concat result "\nSymbol has no properties.")))
    (if (fboundp object)
	(progn
	  (setq result (concat result
			       "\nSymbol has "
			       (describe-function-internal (symbol-function object))))))
    result))

(defun describe-process-internal (object)
  "Return a detailed description of a process object."
  (concat 
   "a process with "
   (if (process-name object)
       (format "a name of %s"
	       (process-name object))
     "no name")
   "."
   (format "\nProcess status is %s, process id is %d."
	   (process-status object)
	   (process-id object))))

(defun describe-window-internal (object)
  "Return a detailed description of a window object."
  (concat
   (if (eq (selected-window) object)
       "the currently selected window"
     "a window")
   "."
   (format "\nWindow's width is %d and height is %d."
	   (window-width object) (window-height object))
   (format "\nWindow starts at character position %d."
	   (window-start object))
  (let ((edges (window-edges object)))
    (format 
     "\nWindow's edges are: top is %d, bottom is %d, left is %d, right is %d."
     (nth 1 edges) (nth 3 edges) (nth 0 edges) (nth 2 edges)))))


(defun describe-function-internal (object)
  "Return a detailed description of a function."
  (concat
   (format "a function value for %s function "
	   (if (commandp object)
	       (if (subrp object)
		   "an interactive built-in"
		 "an interactive")
	     ;;ELSE
	     (if (subrp object)
		 "a built-in"
	       "a")))
   (format "%s" object)
   "."
  (if (documentation object)
      (format "\nFunction has the following documentation:\n  %s\n"
	      (documentation object))
    ;;ELSE
    "")))

(defun describe-functionp (object)
  "Returns t if OBJECT is a function."
  ;; We need to fail on symbols and strings because commandp
  ;; will return t for them.
  (cond ((symbolp object)
	 nil)
	((stringp object)
	 nil)
	((subrp object)
	 ;; We have a built-in function.
	 t)
	((commandp object)
	 ;; We have an interactive function.
	 t)
	((listp object)
	 ;; This should only identify non-interactive, non-built-in functions.
	 (describe-lambdap object))
	(t
	  nil)))

;;; Implementation note:  In Common Lisp a lambda expression is treated as a
;;; list, which is what it really is in GNU Emacs too.  In Common Lisp a
;;; compiled function (i.e. a compiled named lambda expression) is not a list
;;; but is a function.  The nice thing about this that in Common Lisp if one
;;; calls the functionp function and gives it a lambda expression (with a quote
;;; before the initial parenthesis), or gives it a quoted symbol whose function
;;; cell contains a function, or gives it a function object it will return T.
;;; I'm not sure that I agree with this but that is what the Common Lisp
;;; standard indicates.  It says that functionp will return true if its
;;; argument is suitable for applying to arguments.  The real problem here is
;;; that I have not discovered a reasonable way to determine that GNU Emacs has
;;; compiled a function.  The only common denominator is that the function
;;; byte-code is used in GNU Emacs compiled functions.  I haven't found out if
;;; it is always there and if so what positions it can be in.  If the function
;;; is interactive then the byte-code function will not be the top-level form.
;;; For example, try evaluating (symbol-function 'rmail) and compare this withh
;;; (symbol-function 'functionp).
(defun describe-lambdap (object)
  "Return T if OBJECT is a lambda expression."
  (describe-typed-list object 'lambda))

(defun describe-typed-list (object object-type)
  (and (consp object)
       (eq (car object) object-type)
       (consp (cdr object))
       (or (listp (car (cdr object)))
	   (atom (car (cdr object))))))

(defun describe-macrop (object)
  "Returns t if OBJECT is a macro."
  (cond ((listp object)
	 (describe-typed-list object 'macro))
	(t
	  nil)))

(defun describe-macro-internal (object)
  "Return a detailed description of a macro."
  (concat
   (format "a macro.\n")
   (if (documentation object)
       (format "\nMacro has the following documentation:\n  %s\n"
		     (documentation object))
     ;;ELSE
     "")))

(defun describe-autoloadp (object)
  "Returns t if OBJECT is an autoload object."
  (cond ((listp object)
	 (describe-typed-list object 'autoload))
	(t
	  nil)))

(defun describe-autoload-internal (object)
  "Return a detailed description of an autoload object."
  (concat
   (format "an autoload object.\n")
   (if (documentation object)
       (format "\nAutoload object has the following documentation:\n  %s\n"
		     (documentation object))
     ;;ELSE
     "")))



;;; ---------------------------------------------------------------------------
;;; The following code is used to parse keymaps.
;;; ---------------------------------------------------------------------------

(defun describe-basic-keymap-leafp (element)
  "Return t if ELEMENT is a simple keymap leaf."
  (let ((parts element)
	part
	(result nil))
    (if (consp parts)
	(progn
	  (setq result t)
	  (while (and parts result)
	    (if (consp parts)
		(progn
		  (setq part (car parts))
		  (setq result (not (consp part)))
		  (setq parts (cdr parts)))
	      ;;ELSE
	      (setq parts nil)))))
    result))
;;; Test code. Returns (t nil t nil nil).
(list (describe-basic-keymap-leafp '(123 . electric-c-brace))
      (describe-basic-keymap-leafp '(for "For" ([-17] . "  (M-e)") . c-end-of-statement))
      (describe-basic-keymap-leafp '(nil))
      (describe-basic-keymap-leafp 'a)
      (describe-basic-keymap-leafp '(1 2 3 (4 5 6)))
      )

(defun describe-keymap-leafp (element)
  "If this ELEMENT is a keymap leaf return t, otherwise nil"
  ;; A keymap leaf is a list which contains no embedded lists.
  (let ((parts element)
	part
	(result nil))
    (if (and (consp parts)
	     (or (> (safe-length parts) 1)
		 (car parts)))
	(progn
	  (setq result t)
	  (while (and parts result)
	    (if (consp parts)
		(progn
		  (setq part (car parts))
		  (setq result (not (and (consp part)
					 (> (safe-length part) 1))))
		  (setq parts (cdr parts)))
	      ;;ELSE
	      (setq parts nil)))))
    result))

;;; Test code. Should be (t t nil nil nil).
(list (describe-keymap-leafp '(123 . electric-c-brace))
      (describe-keymap-leafp '(for "For" ([-17] . "  (M-e)") . c-end-of-statement))
      (describe-keymap-leafp '(nil))
      (describe-keymap-leafp 'a)
      (describe-keymap-leafp '(1 2 3 (4 5 6)))
      )

(defun describe-collect-leaf-keymaps (keymap)
  "Scan through a KEYMAP and collect just the leaf components."
  (let ((maps keymap)
	map
	(result nil))
    (if (consp maps)
	(while maps
	  (if (consp maps)		; Handle . conses
	      (progn
		(setq map (car maps))
		(if (describe-keymap-leafp map)
		    (progn
		      (setq result (cons map result)))
		  ;;ELSE
		  (if (consp map)
		      (setq result (append result (describe-collect-leaf-keymaps map)))))))
	  (if (consp maps)
	      (setq maps (cdr maps))
	    ;;ELSE
	    (setq maps nil))))
    result))

(defun describe-consolidate-keymap (keymap-list &optional prefix-list)
  "Take the KEYMAP-LIST from describe-collect-leaf-keymaps and generate a linear list of basic keymaps. The output consists of a list of pairs. The first element of a pair is a list of prefixes for this key and the second element is a list of the form (key . function)."
  (let ((result nil)
	keymap)
    (while keymap-list
      (setq keymap (car keymap-list))
      (if (describe-basic-keymap-leafp keymap)
	  (setq result (cons (list prefix-list keymap) result))
	;;ELSE
	(progn
	  (if (numberp (car keymap))
	      (progn
		
		(setq result (append result
				     (describe-consolidate-keymap
				      (cddr keymap)
				      (cons (car keymap) prefix-list))))))))
      (setq keymap-list (cdr keymap-list)))
    result))

(defun describe-keymap-internal (object)
  "Return a detailed description of a keymap."
  (let ((result "a "))
    (if (listp object)
	(setq result (concat result "sparce ")))
    (let ((keymap-length (if (listp object)
			     (apply '+ (mapcar (lambda (a) (if (eq a 'keymap) 0 1)) object))
			   (length object))))
      (setq result (concat result (format "keymap with %d key definitio%s.\n"
					  keymap-length
					  (if (= keymap-length 1)
					      "n"
					    "ns")))))
    (setq result (concat result "Key definitions are:\n"))
    (if (listp object)
	;; This is a sparse keymap.
	(let ((keys (describe-consolidate-keymap (describe-collect-leaf-keymaps object)))
	      pair
	      prefix
	      key
	      function)
	  (while keys
	    (setq pair (car keys)
		  prefix (car pair)
		  key (caadr pair)
		  function (cdadr pair))
	    ;; Convert the prefix list to a string detailing the prefix.
	    (setq prefix 
		  (if (> (length prefix) 0)
		      (apply 'concat
			     (mapcar
			      (lambda (a)
				(concat (describe-character-name a) " "))
			      prefix))
		    ;;ELSE
		    ""))
	    (setq result (concat result
				 prefix
				 (describe-character-name key)
				 "\t"
				 (symbol-name function)
				  "\n"))
	    (setq keys (cdr keys))))
      ;;ELSE
      ;; This is the vector form of a keymap.
      (let ((char-index 0)
	    (keymap-length (length object)))
	(while (< char-index keymap-length)
	  (setq result (concat result "\n"
			       (describe-character-name char-index)
			       "\t"
			       (aref object char-index)))
	  (setq char-index (1+ char-index)))))
    result))

(defun caadr (list)
  "Basic Lisp caadr function."
  (car (car (cdr list))))

(defun cdadr (list)
  "Basic Lisp cdadr function."
  (cdr (car (cdr list))))

(defun describe-character-name (character)
  "Return a the name of a character."
  (cond ((> character ?\ )
	 (char-to-string character))
	((= character 0)
	 "null")
	((= character ?\b)
	 "backspace")
	((= character ?\e)
	 "escape")
	((= character ?\f)
	 "form feed")
	((= character ?\n)
	 "newline")
	((= character ?\r)
	 "return")
	((= character ?\t)
	 "tab")
	((= character ?\v)
	 "vertical tab")
	((= character ? )
	 "space")
	(t
	  (format "control-%c"
		  (- (+ character ?A) 1)))))

