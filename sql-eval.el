;;; sql-related enhancements
;;; http://atomized.org/2008/10/enhancing-emacs%E2%80%99-sql-mode/
;;; support for preset connections

;;; Normally one does something like M-x sql-mysql. This allows one to do M-x
;;; sql-mastermu and not have to enter the connection parameters.

;;; Need emacs version 21.4-a or newer
(if (or (< emacs-major-version 21)
        (and (= emacs-major-version 21)
             (< emacs-minor-version 4)))
    (error "Need to use a more recent version of emacs, using version %d.%d, version 21.4-a or higher is required"
           emacs-major-version emacs-minor-version))
 
(defvar sql-connection-alist
      '((mastermu
         (sql-product 'mysql)
         (sql-server "localhost")
         (sql-user "tekberg")
         (sql-password nil)             ; Force user to enter the first time
         (sql-database "mastermu")
         ;;(sql-port 3306)
         )
        (filemaker-sps
         (sql-product 'mysql)
         (sql-server "1.2.3.4")
         (sql-user "me")
         (sql-password "mypassword")
         (sql-database "thedb")
         (sql-port 3307)))
      "Use this to define your DB parameters."
)

(defun sql-connect-preset (name)
  "Connect to a predefined SQL connection listed in `sql-connection-alist'"
  (eval `(let ,(cdr (assoc name sql-connection-alist))
    (flet ((sql-get-login (&rest what)))
      (if (and (boundp 'sql-password) (not sql-password))
          (progn
            (setq sql-password
                  (sql-read-passwd "Password: " sql-password))
            ;; Stuff the password in the alist.
            (rplacd (assoc 'sql-password (cdr (assoc name sql-connection-alist))) (list sql-password))))
      (sql-product-interactive sql-product)))))

(defun sql-mastermu ()
  (interactive)
  (sql-connect-preset 'mastermu))

;; buffer naming
(defun sql-make-smart-buffer-name ()
  "Return a string that can be used to rename a SQLi buffer.

This is used to set `sql-alternate-buffer-name' within
`sql-interactive-mode'."
  (or (and (boundp 'sql-name) sql-name)
      (concat (if (not(string= "" sql-server))
                  (concat
                   (or (and (string-match "[0-9.]+" sql-server) sql-server)
                       (car (split-string sql-server "\\.")))
                   "/"))
              sql-database)))

(add-hook 'sql-interactive-mode-hook
          (lambda ()
            (setq sql-alternate-buffer-name (sql-make-smart-buffer-name))
            (sql-rename-buffer)))
