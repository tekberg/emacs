;;; Description:
;;;   This code inserts company header information into a new file.  The items
;;;   that are inserted are the CVS Id string and the copyright notice. For .c,
;;;   .C and .cc files, the CVS Id string is suitable for use by the Unix
;;;   "what" program. For C/C++ header files, the macro sequence
;;;   ifndef/define/endif is inserted using the name of the file. After this
;;;   special information is inserted, the cursor is moved to the place where
;;;   editing is expected to start. This code works for all types of files, and
;;;   uses the appropriate comment syntax to insert the CVS Id string and
;;;   copyright notice.
;;;   For .java files, if the file is in a directory structure that contains a
;;;   src directory, a package line is inserted, and the basic class definition
;;;   is also inserted. The cursor is moved to the blank line between the curly
;;;   braces.
;;;
;;; Installation:
;;;   Add a load command to your .emacs (or related equivalent) file. Something
;;;   like the following:
;;;     (load "/usr/local/lib/emacs/command/insert-header.elc" nil t)
;;;
;;; Usage:
;;;   When using the find-file command and the file does not exist, the user
;;;   will be asked if the company header is to be inserted.  Entering "y" or
;;;   space indicates that the header is to inserted. Entering "n" or delete
;;;   indicates that the file should be left blank.
;;;


(defconst company-name "University of Washington Laboratory Medicine")
;;;(defconst company-name "MRG")

;;; Copyright notice. It is in list form to make the special handling of the
;;; first, middle and last lines easier. Note that the first line starts with a
;;; space and the last line ends with a space.
(defconst company-copyright 
  (let* ((time-string (current-time-string))
	 ;;  012345678901234567890123
	 ;; "Thu Nov  5 10:42:45 1992"
	 (year  (substring time-string 20)))
    (list
     " *********************************************************************"
     ""
     (format "   Copyright (c) %s %s" year company-name)
     "   All Rights Reserved"
     ""
     "   The information contained herein is confidential to and the"
     (format "   property of %s and is" company-name)
     "   not to be disclosed to any third party without prior express"
     (format "   written permission of %s." company-name)
     (format "   %s, as the" company-name)
     "   author and owner under 17 U.S.C. Sec. 201(b) of this work made"
     "   for hire, claims copyright in this material as an unpublished "
     "   work under 17 U.S.C. Sec.s 102 and 104(a)   "
     ""
     " *********************************************************************")))


(defun insert-company-header ()
  "Insert company header into a new file."
    (if (and (not (string-match "/SA\$" (directory-file-name (file-name-directory (buffer-file-name)))))
             (y-or-n-p (format "New file. Insert %s header? " company-name)))
	(progn
	  ;; Unfortunately the mode setting isn't done until later. Since we
	  ;; need the comment syntax we take care of loading it now.
	  (let ((alist auto-mode-alist)
		(mode nil))
	    (while alist
	      (if (string-match (car (car alist)) buffer-file-name)
		  (setq mode (cdr (car alist))
			alist nil))
	      (setq alist (cdr alist)))
	    (if mode
		(progn
		  (funcall mode))))
	  ;; Position after anything that the mode code may have loaded. For
	  ;; shell mode (.sh files) the line "#! /bin/ksh" may be added.
	  (goto-char (point-max))
	  (let* ((header-other "  @(#) $Id:  $  ")
		 (header-dot-c "static char rcsid[] = \"@(#) $Id:  $\";
static char *rcs_dummy[] = {rcs_dummy[0], rcsid};")
		 ;; Some comment settings have a leading/trailing blank and
		 ;; some do not. We remove the leading/trailing blank so we
		 ;; handle them all the same.
		 (comment-end-string (if (and (> (length comment-end) 0)
					      (= (aref comment-end 0) ? ))
					 (substring comment-end 1)
				       ;;ELSE
				       comment-end))
		 (comment-start-string (if (and (> (length comment-start) 1)
						(equal (substring comment-start
								  (- (length comment-start) 1)
								  (length comment-start))
						       " "))
					   (substring comment-start 0 (- (length comment-start) 1))
					 ;;ELSE
					 (or comment-start ""))))
	    (if (or (string-match "\\.c$"  buffer-file-name)
		    (string-match "\\.cc$" buffer-file-name))
		(insert header-dot-c)
	      ;;ELSE
	      ;; Have a non-C or C++ file. Put a comment in for CVS.
	      (insert comment-start-string header-other comment-end-string))
	    (insert "\n" "\n")
	    ;; Now insert the copyright notice.
	    (if (= (length comment-end-string) 0)
		;; Do not have a comment end. Need to put a comment start at the
		;; beginning of each line.
		(apply 'insert 
		       (mapcar '(lambda (a) 
				  (if (= (length comment-start-string) 1)
				      (concat comment-start-string
					      comment-start-string
					      comment-start-string a "\n")
				    ;;ELSE
				    (concat comment-start-string " " a "\n")))
			       company-copyright))
	      ;;ELSE
	      ;; Have both a comment start and a comment end. Put the comment
	      ;; start on the first line, some nice punctuation at the beginning
	      ;; of the other lines, and a comment end at the end of the last
	      ;; line.
	      (insert 
	       comment-start-string (car company-copyright)
	       (apply 'concat (mapcar '(lambda (a) (concat "\n *" a)) (cdr company-copyright)))
	       comment-end-string))
	    (insert "\n" "\n")
	    (if (or (string-match "\\.h$"  buffer-file-name)
		    (string-match "\\.hh$" buffer-file-name))
		;; Now do the ifndef, etc. macros.
		(let ((base-name (file-name-nondirectory buffer-file-name))
		      (case-fold-search nil)
		      match-loc next-loc start-here)
		  ;; Change dots to underscores.
		  (while (setq match-loc (string-match "\\." base-name))
		    (aset base-name match-loc ?_))
		  ;; Change dashes to underscores.
		  (while (setq match-loc (string-match "\\-" base-name))
		    (aset base-name match-loc ?_))
		  ;; Insert _ before capital letter.
		  (setq next-loc 1)
		  (while (setq match-loc (string-match "[A-Z]" base-name next-loc))
		    (if (or (= match-loc 0)
			    (not (equal ?_ (aref base-name (- match-loc 1)))))
			(progn
			  (setq base-name (concat
					   (substring base-name 0 match-loc)
					   "_"
					   (substring base-name match-loc)))))
		    (setq next-loc (+ match-loc 2)))

		  (setq base-name (concat "_" (upcase base-name)))
		  (insert "#ifndef " base-name "\n"
			  "#define " base-name "\n" "\n")
		  (setq start-here (point))
		  (insert "\n" "\n"
			  "#endif /* Do not add anything after this line. */\n")
		  (goto-char start-here)))
            ;; For Java files that are in directory hierarchy that contains a
            ;; "src" directory, insert the package name.
	    (if (string-match "\\.java$"  buffer-file-name)
                (progn
                  (if (string-match "/src/"  buffer-file-name)
                      (let* ((dirname (file-name-directory buffer-file-name))
                             (match-loc (string-match-last "/src/" dirname))
                             (package-name (string-replace
                                            (substring dirname (+ match-loc 5) -1)
                                            "/" ".")))
                        (insert "package " package-name ";\n\n\n")))
                  ;; Insert the class definition for all Java files.
                  (let ((class-name (file-name-sans-extension
                                     (file-name-nondirectory buffer-file-name)))
                        start-here)
                    (insert "public class " class-name "\n{\n")
                    (setq start-here (point))
                    (insert "\n}\n")
                    (goto-char start-here))))))))

(defun string-match-last (regexp string)
  "Return the index of the last match of REGEXP within STRING."
  (let ((loc -1)
        match-loc
        (had-match nil))
    (if (string-match regexp string)
        (progn
          (setq match-loc -1
                had-match t)
          (while had-match
            (setq loc (+ match-loc 1)
                  match-loc (string-match regexp string loc)
                  had-match  (not (null match-loc))))
          (- loc 1))
      ;;ELSE
      nil)))

(add-hook 'find-file-not-found-hooks 'insert-company-header)
