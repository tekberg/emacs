;;; This implements the electric comments (*/) for C mode, for C++ mode and for
;;; Java mode.

;;; The idea is that if ending comment indicator (*/) is entered without
;;; leading whitespace then the comment is swung out to the right margin.
;;; If there is intervening whitespace then the comment ending indicator is
;;; left as is.

(if (not c-mode-map)
  (setq c-mode-map (make-sparse-keymap)))

(define-key c-mode-map "/" 'electric-c-comment)

(add-hook 'c++-mode-hook 'turn-on-electric-c++-comment)
(add-hook 'java-mode-hook 'turn-on-electric-java-comment)

(defun turn-on-electric-c++-comment ()
  "Turn on the electric comment stuff when C++ mode is loaded."
  (if c++-mode-map
      (progn
	(define-key c++-mode-map "/" 'electric-c-comment))))
      

(defun turn-on-electric-java-comment ()
  "Turn on the electric comment stuff when Java mode is loaded."
  (if java-mode-map
      (progn
	(define-key java-mode-map "/" 'electric-c-comment))))
      

(defconst c-mode-electric-comment-regex
  "[^ \t]\\*"
  "Regular expression for characters preceeding the slash that cause it to be
moved.")

;;; If someone wants to change this variable, make the change only for that
;;; buffer and not all buffers.
(make-variable-buffer-local 'c-mode-electric-comment-regex)

(defun electric-c-comment (arg)
  "Move a comment ending indicator (*/) to the right margin, sometimes."
  (interactive "P")
  (insert "/")
  (save-excursion
    (if (not (bobp))
	(progn
	  (backward-char)		;Looking at /
	  (if (not (bobp))
	      (progn
		(backward-char)		;Looking at *
		(if (not (bobp))
		    (progn
		      (backward-char)	;looking at char before *
		      (if (looking-at c-mode-electric-comment-regex)
			  (progn
			    (forward-char)
			    (let (movement
				  (old-position (current-column))
				  line-end)
			      (end-of-line)
			      (setq movement (- fill-column (current-column)))
			      (if (< movement 0)
				  (setq movement 0))
			      (move-to-column old-position)
			      (indent-to (+ old-position movement)))))))))))))
