;;; -*-Mode:Lisp-Interaction -*-

;;; Indent the text after point so that the rightmost character is at the
;;; column indicated by the fill column.
;;; This is useful in C comments when you want to put the comment close
;;; sequence at the right edge.

(defun indent-to-edge ()
  "Indent text after point to put the right most character at the fill column."
  (interactive)
  (let (movement
	(old-position (current-column))
	line-end)
    (end-of-line)
    (setq movement (- fill-column (current-column)))
    (if (< movement 0)
	(setq movement 0))
    (move-to-column old-position)
    (indent-to (+ old-position movement))))
