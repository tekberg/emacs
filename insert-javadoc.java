````// Test cases for insert-javadoc (splat-javadoc)
public class {
    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @return
     *   String[] - 
     */
    //-------------------------------------------------------------------------
    public String[] foo() {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  sql  
     *
     * @return
     *   List<String> - 
     */
    //-------------------------------------------------------------------------
    public List<String> parseWhere(String sql) {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  parameterNames  
     *
     * @param  isStatic  
     */
    //-------------------------------------------------------------------------
    public void MethodInfo(Vector<String> parameterNames, boolean isStatic) {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @return
     *   MethodInfo - 
     */
    //-------------------------------------------------------------------------
    protected <T extends AccessibleObject&Member> MethodInfo generateMethodSignature() {
    }

    public Hashtable<String, String> splitToHashtable(String separator)
    {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  bar  
     *
     * @param  baz  
     */
    //-------------------------------------------------------------------------
    <T extends Map&Comparable> <V extends Map&Number> void foo(T bar, V baz) {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  key  
     *
     * @param  value  
     *
     * @return
     *   Map<S, T> - 
     */
    //-------------------------------------------------------------------------
    public static <S, T> Map<S, T> map(S key, T value) {
    }


    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @return
     *   List<List<E>> - 
     */
    //-------------------------------------------------------------------------
    public List<List<E>> foo() {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  a  
     *
     * @param  b  
     *
     * @param  c  
     *
     * @return
     *   List<List<E>> - 
     */
    //-------------------------------------------------------------------------
    public List<List<E>> foo(List<List<E>> a, List<List<E>> b, Map<S, T>c) {
    }

    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @return
     *   ArrayList - 
     */
    //-------------------------------------------------------------------------
    public static ArrayList getTracesWithNullFilterStatus() {
    }
    
    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  trace  
     */
    //-------------------------------------------------------------------------
    synchronized public static void buildNameTableFromTraceData( int trace ) {
    }


    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  traceID  
     *
     * @return
     *   boolean - 
     */
    //-------------------------------------------------------------------------
    public static boolean isFilterStatusNull( int traceID ) {
    }


    //-------------------------------------------------------------------------
    /**
     * 
     *
     * @param  traceId  
     *
     * @param  newStatus  
     */
    //-------------------------------------------------------------------------
    public static void setFilterStatus( int traceId, int newStatus ) {
    }
}
