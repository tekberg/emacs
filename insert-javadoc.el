;;; -*-Mode:Lisp-Interaction -*-

;;; This code takes a method such as:
;;;      int getCounter(Data p_data1, String p_data2) throws FooException
;;;      {
;;;         ...
;;;         return something;
;;;      }
;;; and inserts JavaDoc comments so it looks like:
;;;     //---------------------------------------------------------------------
;;;     /**
;;;      * 
;;;      *
;;;      * @param  p_data1  
;;;      *
;;;      * @param  p_data2  
;;;      *
;;;      * @return
;;;      *   int - 
;;;      *
;;;      * @throws FooException 
;;;      */
;;;     //---------------------------------------------------------------------
;;;      int getCounter(Data p_data1, String p_data2) throws FooException
;;;      {
;;;         ...
;;;         return something;
;;;      }
;;;
;;; The following syntactic elements are handled specially:
;;;
;;;   constructor,
;;;   static initializer and
;;;   method.
;;;
;;; For a constructor or a method the following things collected and inserted
;;; into their proper place in the JavaDoc:
;;;
;;;   parameter names
;;;   return type 
;;;   exceptions thrown
;;;
;;; If the return type is not "void" then it is inserted into its proper place.
;;;
;;; After generating the JavaDoc, the cursor is positioned at an appropriate
;;; place and a message is displayed at the bottom of the screen indicating
;;; what was found. If an expected syntactic element is not found, "Unknown" is
;;; displayed and a simple /** */ JavaDoc comment is generated.
;;;
;;; To install you can bind it to a key.  To use, position somewhere before the
;;; syntactic element, enter the key, or M-x insert-javadoc. At that point you
;;; can enter the description of the syntactic element, and move down to enter
;;; the descriptions of the parameters, return type and throws, if present.
;;;
;;; Example code to bind to a key. Change this to what you want and remove
;;; the ;;; comment characters.
;;;(global-set-key "\eJ" 'insert-javadoc)


(defun insert-javadoc ()
  "Insert JavaDoc comments for a Java syntactic element."
  (interactive)
  (let (starting-point                  ;Where scanning starts
	start				;Start of some thing
        end                             ;End of something
	end-parameters			;Position of closed paren
	return-type			;Return type of method
	(parameters nil)		;All parameters
        (throws nil)                    ;Thrown exception.
	start-parameters		;Where parameters start
	loc-comma			;Position of comma in parameter list
        loc-lparen                      ;Position of left paren
        loc-lcbrace                     ;Position of left curly brace
        loc-semi                        ;Position of semicolon
        method-name                     ;Name of the method
        (thing-found "Unknown")         ;Used to say what was found
        (is-constructor nil)            ;T if a constructor was found
	)
    (beginning-of-line)
    (setq starting-point (point))
    ;; The first step is to distinguish a method or constructor from a static
    ;; initializer.
    (setq loc-lparen (search-forward "(" nil t)) ;Move just after method name
    (setq start (point))
    (goto-char starting-point)          ;Go back again
    (setq loc-semi (search-forward ";" nil t)) ;Move after a semicolon
    (goto-char starting-point)          ;Go back again
    (setq loc-lcbrace (search-forward "{" nil t)) ;Move after a left curly brace
    (if (and (not (null loc-lparen))
             (not (null loc-lcbrace))
             (< loc-lparen loc-lcbrace)
             (or (null loc-semi)
                 (> loc-semi loc-lcbrace)))
        (progn
          ;; We found a method or a constructor.
          (goto-char start)             ;Move just after method name
          (backward-char)               ;Move before paren
          (backward-sexp 1)             ;Move to start of the method name
          (setq start (point))
          (forward-sexp 1)              ;Move to end of the method name
          (setq method-name (buffer-substring start (point)))
          (if (equal method-name (file-name-nondirectory
                                 (file-name-sans-extension
                                  (buffer-file-name))))
              (progn
                ;; The method name and file name match, so this must be a
                ;; constructor. This doesn't work for inner class
                ;; constructors.
                (setq is-constructor t))
            ;;ELSE
            ;; We have a method with a return type.
            (backward-sexp 1)           ;Move to start of the method name
            (backward-char)             ;Just before the method name
            ;; Scan for the end of the return type.
            ;; Careful positioning has to be done so that array return types
            ;; are handled correctly.
            (setq return-type (ij-scan-return-type)))

          ;; Extract all of the parameter names.
          (search-forward "(")		;Move just after method name
          (setq start-parameters (point))
          (setq end-parameters (search-forward ")"))
          (goto-char start-parameters)
          (while (and (setq loc-comma (ij-scan-forward-parameter))
                      (<= loc-comma end-parameters))
            (backward-char 2)             ;Move before delimiter
            (if (looking-at "(")
                (forward-char 2)          ;Empty parameter list
              ;;ELSE
              (forward-char)
              (backward-sexp)             ;Move before parameter name
              (setq start (point))
              (forward-sexp)              ;Move over parameter name
              (setq parameters (cons (buffer-substring start (point)) parameters))
              (goto-char loc-comma)))      ;Skip to comma again
          ;; Order parameters in the right order: first to last.
          (setq parameters (reverse parameters))

          ;; Now look for a "throws" keyword.
          (goto-char end-parameters)
          (ij-forward-skip-white)
          (if (looking-at "throws")
              (progn
                (forward-sexp 2)        ;End of exception class
                (setq end (point))
                (backward-sexp 1)       ;Start of exception class
                (setq throws (cons (buffer-substring (point) end) throws))
                (goto-char end)         ;End of exception class
                (ij-forward-skip-white) ;Skip over any whitespace 
                ;; Look for more exception classes.
                (while (looking-at ",")
                    (progn
                      ;; Found a comma which signifies that another exception
                      ;; class follows.
                      (forward-sexp 1)      ;Skip over exception class
                      (setq end (point))
                      (backward-sexp 1)     ;Start of exception class
                      (setq throws (cons (buffer-substring (point) end)
                                         throws))
                      (goto-char end)       ;End of exception class
                      ;; Position to where a comma might be placed.
                      (ij-forward-skip-white)))))
          ;; Sort the throws list so @throws are ordered alphabetically.
          (setq throws (sort throws 'string-lessp))

          ;; Identify what was found.
          (setq thing-found
                (apply 'format "%s with %s%s%s"
                       (list
                        (if is-constructor
                            "Constructor"
                          "Method")
                        ;; How many parameters are there.
                        (let ((n-parameters (length parameters)))
                          (apply 'format "%s parameter%s"
                                 (list
                                  (if (zerop n-parameters)
                                      "no"
                                    ;;ELSE
                                    (format "%d" n-parameters))
                                   
                                  (if (= n-parameters 1)
                                      ""
                                    "s"))))
                        ;; Return type.
                        (if is-constructor
                            ""
                          (if (equal return-type "void")
                              " and no return value"
                            ;;ELSE
                            (concat " returning " return-type)))
                        (if throws
                            (concat " throws " (ij-join "," throws))
                          ""))))

          ;; At this point we have the parameter names, return type and thrown
          ;; exceptions.
          (ij-generate-method-javadoc starting-point is-constructor
                                      parameters return-type throws))
      ;;ELSE
      (progn
        ;; Point is just after the left curly brace.
        ;; Scan a for a static initializer.
        (backward-char)               ;Move before left curly brace
        (backward-sexp 1)             ;Move to static keyword
        (if (looking-at "static")
            (progn
              (setq thing-found "Static initializer")
              (ij-generate-static-initializer-javadoc starting-point))
          ;;ELSE
          ;; Don't know about this syntactic element.
          (ij-generate-simple-javadoc starting-point))))
    (message thing-found)))


(defun ij-scan-return-type ()
  "Scan for the return type and return it. It is assumes that point is positioned just before the method name."
  (let (end)
    (ij-backward-skip-white)
    (forward-char)                      ;Just after the return type
    (setq end (point))                  ;End of the return type
    (backward-char)                     ;Last character of return type
    (if (looking-at ">")
        (ij-scan-generic-type)
      ;;ELSE
      (progn
        (forward-char)                  ;Just after the return type
        (backward-word 1)               ;First word of the return type
        (forward-char)                  ;Before first word of return type
        (backward-sexp 1)))             ;Start of return type
    (buffer-substring (point) end)))


(defun ij-scan-generic-type ()
  "Scan a generic type and move point at the start of the return type.
On entry, point is positioned at the trailing >."
  (let ((n-matching 1))
    (while (> n-matching 0)
      (backward-char)
      (if (looking-at ">")
          (setq n-matching (+ n-matching 1))
        ;;ELSE
        (if (looking-at "<")
          (setq n-matching (- n-matching 1))))))
  (backward-sexp 1))


(defun ij-scan-forward-parameter ()
  "Scan forward looking for the end of this parameter."
  (let ((keep-looking t)
        (n-matching 0))
    (while keep-looking
      (if (looking-at ">")
          (setq n-matching (+ n-matching 1))
        ;;ELSE
        (if (looking-at "<")
          (setq n-matching (- n-matching 1))))
      (if (or
           (and (= n-matching 0) (looking-at ","))
           (looking-at ")"))
            (setq keep-looking nil))
      (forward-char)))
  (point))


(defun ij-generate-method-javadoc (starting-point
                                   is-constructor
                                   parameters return-type throws)
  "Generate the JavaDoc for a method or a constructor.
STARTING-POINT is the beginning of the line to contain the JavaDoc.
IS-CONSTRUCTOR is T if this is a constructor, nil if a method.
PARAMETERS is a list of the parameter names.
RETURN-TYPE is a string containing the return type.
THROWS is a list of the thrown exceptions."
  (let (start
        parameter)
    (goto-char starting-point)
    (ij-insert-dashes)
    (insert "\n")
    (insert "/**")
    (c-indent-command)                  ; Align the /**
    (insert "\n")
    (insert "*")
    (c-indent-command)                  ; Align the *
    (insert " ")
    (setq start (point))                ; Description goes here
    (while parameters
      (setq parameter (car parameters))
      (insert "\n*")
      (c-indent-command)                ; Align the *
      (insert (format "\n* @param  %s  " parameter))
      (c-indent-command)                ; Align the * @param
      (setq parameters (cdr parameters)))
    ;; Now process the return type.
    ;; Don't show a return type for constructors and methods returning
    ;; void.
    (if (and (not is-constructor)
             (not (equal return-type "void")))
        (progn
          (insert "\n*")
          (c-indent-command)            ; Align the *
          (insert "\n* @return")
          (c-indent-command)            ; Align the * @return
          (insert "\n")
          (insert (format "*   %s - " return-type))
          (c-indent-command)))          ; Align the * return-type
    (if throws
        (mapcar (lambda (throw)
                  (insert "\n*")
                  (c-indent-command)    ; Align the *
                  (insert"\n* @throws " throw " ")
                  (c-indent-command))   ; Align the * @throws
                throws))
    (insert "\n*/")
    (c-indent-command)                  ; Align the */
    (forward-char)
    (ij-insert-dashes)
    ;; Position to description.
    (goto-char start)))


(defun ij-generate-static-initializer-javadoc (starting-point)
  "Generate the JavaDoc for a static initializer.
STARTING-POINT is the beginning of the line to contain the JavaDoc."
  (let (start)
    (goto-char starting-point)
    (ij-insert-dashes)
    (insert "\n")
    (insert "/**")
    (c-indent-command)                  ; Align the /**
    (insert "\n* Static initializer that ")
    (c-indent-command)                  ; Align the *
    (setq start (point))                ; Description goes here
    (insert "\n*/")
    (c-indent-command)                  ; Align the */
    (forward-char)
    (ij-insert-dashes)
    ;; Position to description.
    (goto-char start)))


(defun ij-generate-simple-javadoc (starting-point)
  "Generate a simple JavaDoc.
STARTING-POINT is the beginning of the line to contain the JavaDoc."
  (let (start)
    (goto-char starting-point)
    (insert "\n")
    (backward-char)
    (c-indent-command)                  ; Align the /**
    (insert "/**\n")
    (insert "* ")
    (c-indent-command)                  ; Align the *
    (setq start (point))                ;Place the cursor here when we are done
    (insert "\n")
    (insert "*/")
    (c-indent-command)                  ; Align the */
    (goto-char start)))


(defun ij-forward-skip-white ()
  "Move forward skipping over whitespace."
  (while (looking-at "[\t\n\r ]")
    (forward-char)))


(defun ij-backward-skip-white ()
  "Move backward skipping over whitespace."
  (while (looking-at  "[\t\n\r ]")
    (backward-char)))


(defun ij-to-string (a)
  "Convert something into a string.
A is the thing to convert into a string."
  (format "%s" a))


(defun ij-join (separator sexp)
  "Join elements of a list together, with the element separated by a separator.
SEPARATOR is the element separator.
SEXP contains the elements to be joined.

This is much like the Perl join operator."
  (if (> (length sexp) 1)
      (progn
      (let ((head (car sexp))
            (rest (cdr sexp)))
        (concat (ij-to-string head)
                (apply 'concat
                       (mapcar (lambda (a)
                                 (concat separator (ij-to-string a)))
                               rest))))
      )
    ;;ELSE
    (ij-to-string (car sexp))))


(defun ij-insert-dashes ()
  "Insert a line of dashes for inside a // style comment.
Takes care to not insert too many dashes."
  (insert "\n")
  (backward-char)
  (insert "//")
  (c-indent-command)                    ; Align the //
  (let* ((col (current-column))
         (dash-count (- 79 col)))
    (insert (make-string dash-count ?-))))


;;; Don't know why this is needed, but the indentation code complains if it has
;;; no value. You could try it with this commented out to see how it works for
;;; you.  Instead of indenting, one could indent by a fixed amount.
(setq c-lineup-C-comments nil)
