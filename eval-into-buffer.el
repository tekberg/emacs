;;; -*-Mode:Lisp-Interaction -*-

;;; This file defines the command `eval-into-buffer' which is similar to the
;;; Zmacs command of the same name.  The use of this function is evaluate a
;;; lisp expression and put the result returned into the buffer.

(defun eval-into-buffer ()
  "Prompt the user for an expression and put that into the buffer."
  (interactive)
  (let* ((exp (read-from-minibuffer "Enter expression: "))
	 (result (eval (car (read-from-string exp))))
	 (buf-name (symbol-name (gensym)))
         (current-config (current-window-configuration)))
    (with-output-to-temp-buffer buf-name
      (print result))
    (let ((standard-output t))
      (insert-buffer-substring (get-buffer buf-name) 2))
    (kill-buffer buf-name)
    (set-window-configuration current-config)))
