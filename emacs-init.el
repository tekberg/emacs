;;; -*-Mode:Lisp-Interaction -*-

(defvar emacs-drive
      (if (file-directory-p "c:/cygwin")
          "c:"
        ;;else
        (if (file-directory-p "e:/cygwin")
            "e:"
          ;;else
          "")))

(defconst unix-p (or (string-match "unix" (symbol-name system-type))
		     (string-match "linux" (symbol-name system-type)))
  "T If emacs is running on Unix or Linux, nil otherwise.")

(defvar emacs-home (format (if unix-p
                               "%s/usr/local/share/emacs-%d.%d"
                             ;;else
                             "%s/emacs-%d.%d")
                           emacs-drive emacs-major-version emacs-minor-version))

;;; Used to create a txt file of a WMV file in a directory listing.
;;; M-< C-S .wmv C-N C-P C-SPC C-E M-B M-W C-X C-F C-Y txt RETURN   BACKSPACE C-X C-S
(setq last-kbd-macro [escape 60 19 46 119 109 118 14 16 67108896 5 escape 98 escape 119 24 6 25 116 120 116 return 32 backspace 24 19])

;;; Don't remember what this is for. The characters expand to the following:
;;; C-A C-SPC M-f C-W M-x eval in b RETURN (format \"%02d\" C-Y ) RETURN BACKSPACE BACKSPACE C-A C-D M-f M-f C-SPC M-b C-W M-x eval in b RETURN (format \"%02d\" C-Y ) RETURN BACKSPACE BACKSPACE M-b BACKSPACE M-b M-f M-f M-t M-b M-b M-t M-b BACKSPACE - M-f C-D - M-f M-\\ C-D M-\\   M-c M-c M-c M-c M-C-B C-A
;;;(setq last-kbd-macro [1 67108896 escape 102 23 escape 120 101 118 97 108 32 105 110 32 98 32 return 40 102 111 114 109 97 116 32 34 37 48 50 100 34 32 25 41 return backspace backspace 1 4 escape 102 escape 102 67108896 escape 98 23 escape 120 101 118 97 108 32 105 110 32 98 32 return 40 102 111 114 109 97 116 32 34 37 48 50 100 34 32 25 41 return backspace backspace escape 98 backspace escape 98 escape 102 escape 102 escape 116 escape 98 escape 98 escape 116 escape 98 backspace 45 escape 102 4 45 escape 102 escape 92 4 escape 92 32 escape 99 escape 99 escape 99 escape 99 escape 2 1])
;;; Used the following to show what is in last-kbd-macro
;;;(defun foo (a)
;;;  (concat " " (if (symbolp a)
;;;                  (format "%S" a)
;;;                (if (< a 32)
;;;                    (format "C-%c" (+ 64 a))
;;;                  (if (= a 67108896)
;;;                      "C-SPC"
;;;                    (format "%c" a a))))))
;;;(apply 'concat (mapcar 'foo last-kbd-macro))


;;; Display the whole expression.
(setq eval-expression-print-length nil)
;; ----------------------------------------------------------------------------
;; Start of JDEE customizations.
;; Uncomment the lines that start with ;;;TWE when the JDEE files are
;; available.

;; Update the Emacs load-path to include the path to
;; the JDE and its require packages. This code assumes
;; that you have installed the packages in the emacs/site
;; subdirectory of your home directory.
;;;TWE(add-to-list 'load-path (expand-file-name "~/jde-2.3.5/lisp"))
;;;TWE(add-to-list 'load-path (expand-file-name "~/cedet-1.0beta3b/common"))
;;;TWE(add-to-list 'load-path (expand-file-name "~/EMACS/elib-1.0"))

;; Initialize CEDET.
;;;TWE(load-file (expand-file-name "~/cedet-1.0beta3b/common/cedet.el"))

;; I want Emacs to defer loading the JDE until I open a Java file.
;;;TWE(defvar defer-loading-jde t)

;;;TWE(if defer-loading-jde
;;;TWE    (progn
;;;TWE      (autoload 'jde-mode "jde" "JDE mode." t)
;;;TWE      (setq auto-mode-alist
;;;TWE	    (append
;;;TWE	     '(("\\.java\\'" . jde-mode))
;;;TWE	     auto-mode-alist)))
;;;TWE  (require 'jde))

;; Setup Emacs to run bash as the primary shell.
;;;TWE(setq shell-file-name "bash")
;;;TWE(setq shell-command-switch "-c")
;;;TWE(setq explicit-shell-file-name shell-file-name)
;;;TWE(setenv "SHELL" shell-file-name)
;;;TWE(setq explicit-sh-args '("-login" "-i"))

;; End of JDEE customizations.
;; ----------------------------------------------------------------------------


;;;Some redefinitions for EMACS.

(if (not unix-p)
    ;; chmod doesn't work on Windows-NT since it can't find it for some
    ;; reason. Pointing to it directly makes it work.
    (setq dired-chmod-program
          (format "%s/cygwin/bin/chmod" emacs-drive)))

;;; Set to nil to not see the ^M (DOS text mode), set to t to see them.
(setq inhibit-eol-conversion nil)

;;; Set this number high so that line-number mode will display the
;;; line number even when there are long lines.
(setq line-number-display-limit-width 5000)


;;;(set-frame-height (selected-frame) 60)

;;; Required to get M-x man to work.
(defvar manual-formatted-dirlist nil)

;;; Control processing of the "variable" `eval` in a file`s local
;;; variables.  A value of t means obey `eval` variables.  This is needed
;;; for Emacs version 19.
(setq enable-local-eval t)
(global-set-key "\eg" 'fill-region)

;;; Define register `-' for use in email messages.
(set-register ?- "-------------------------------------------------------------------------------\n")
(set-register ?f " type=F")
(set-register ?g " type=G")
(set-register ?l " type=L")
(set-register ?p " type=PO")

(defun set-process-environment (env-name value)
  "Set or change the value of an environment variable."
  (let* ((env-list process-environment)
	 (new-env-list nil)
	 (env-name-was-set nil)
	 (env-list-element nil)
	 (env-name (concat env-name "="))
	 (env-name-length (length env-name)))
    (while (> (length env-list) 0)
      (setq env-list-element (car env-list))
      (if (and (>= (length env-list-element) env-name-length)
	       (equal (substring env-list-element 0 env-name-length) env-name))
	  (progn
	    (setq env-name-was-set t)
	    (setq env-list-element (concat env-name value))))
      (setq new-env-list (cons env-list-element new-env-list))
      (setq env-list (cdr env-list)))
    (if (not env-name-was-set)
	;; For some reason the environment variable wasn't there.
	(setq new-env-list (cons (concat env-name value) new-env-list)))
    (setq process-environment (reverse new-env-list)))
  nil)

(defun get-process-environment (env-name)
  "Get the value of an environment variable."
  (let* ((env-list         process-environment)
	 (new-env-list     nil)
	 (env-value        nil)
	 (env-name-was-set nil)
	 (env-list-element nil)
	 (env-name	   (concat env-name "="))
	 (env-name-length  (length env-name)))
    (while (and (> (length env-list) 0) (not env-name-was-set))
      (setq env-list-element (car env-list))
      (if (and (>= (length env-list-element) env-name-length)
	       (equal (substring env-list-element 0 env-name-length) env-name))
	  (progn
	    (setq env-name-was-set t)
	    (setq env-value (substring env-list-element env-name-length))))
      (setq new-env-list (cons env-list-element new-env-list))
      (setq env-list (cdr env-list)))
    env-value))

(defun string-replace (string from replace)
  "Replace in STRING occurrences of FROM with REPLACE."
  (let ((start 0)
        (diff (- (length replace) (length from)))
	begin end)
    (while (string-match from string start)
      (setq begin (match-beginning 0))
      (setq end   (match-end 0))
      (setq string (concat (substring string 0 begin)
			   replace
			   (substring string end)))
      (setq start (+ end diff)))
    string))

;;; Enable the ESC ESC sequence for expression evaluation.
(global-set-key "\e\e" 'eval-expression)

;;; Enable the ESC ESC sequence for expression evaluation.
(put 'eval-expression 'disabled nil)

;;; Fix up the PATH environment variable so it works for Unix and Windows NT.
(set-process-environment
 "PATH"
 (let ((dirs 
        (if unix-p
            '("/usr/local/java/jdk2/bin"
              "$HOME/BIN"               ;; Try both of these
              "$HOME/bin"
              "/usr/X/bin"
              "/usr/local/wind/host/sun4-solaris2/bin"
              "/usr/ccs/bin"
              "/opt/SUNWspro/bin"
              "/usr/ucb"
              "/usr/local/bin"
              "/usr/sbin"
              "/usr/bin"
              "/bin"
              "/etc"
              "."
              "/opt2/isippc/pssppc/bin/solaris"
              "/home/cp/bin"
              "/home/devel/bin"
              "/usr/openwin/bin")
          ;;ELSE
          (cons
           (concat (substitute-in-file-name "$JAVA_HOME") "/bin")
           (list (format "%s/cygwin/bin"    emacs-drive)
            (format "%s/ant/bin"            emacs-drive)
            (format "%s/j2sdkee1.2/bin"     emacs-drive)
            (format "%s/jakarta-tomcat/bin" emacs-drive)
            (format "%s/bin"                emacs-drive)
            (format "%s/usr/local/bin"      emacs-drive)
            (concat emacs-home "/bin")))))
       (search-path "")
       (sep (if unix-p ":" ";"))
       dir)
   (while dirs
     (setq dir (car dirs))
     (if (not unix-p)
         ;; Change / to \ for windows.
         (setq dir (string-replace dir "/" "\\")))
     (if (file-directory-p (substitute-in-file-name dir))
         (setq search-path 
               (if (> (length search-path) 0)
                   (concat search-path sep (substitute-in-file-name dir))
                 ;;ELSE
                 (substitute-in-file-name dir))))
     (setq dirs (cdr dirs)))
   search-path))

;;; Fix up the MANPATH environment variable.
(set-process-environment "MANPATH"
  (if unix-p
      "/home/tekberg/MAN:/usr/openwin/man:/usr/man:/usr/local/man:/opt/SUNWspro/man:/opt/SUNWrtvc/man:/opt2/flexlm/man"
    ;;ELSE
    (format "%s/cygwin/usr/share" emacs-drive)))

;; Allow diff to use dos file names.
(if (not unix-p)
    (set-process-environment "CYGWIN"
      "nodosfilewarning"))

;;; Turn off the automatic block commenting in C code when auto-fill is
;;; turned on.
(setq comment-multi-line t)
(setq-default comment-column 40)

;;; Taken from cl.el.
(defun endp (x)
  "t if X is nil, nil if X is a cons; error otherwise."
  (if (listp x)
      (null x)
    (error "endp received a non-cons, non-null argument `%s'"
	   (prin1-to-string x))))

;;; Modified from cl.el.  Same as memq but uses equal instead of eq.
(defun member (item list)
  "Look for ITEM in LIST; return first link in LIST whose car is `equal' to ITEM."
  (let ((ptr list)
        (done nil)
        (result '()))
    (while (not (or done (endp ptr)))
      (cond ((equal item (car ptr))
             (setq done t)
             (setq result ptr)))
      (setq ptr (cdr ptr)))
    result))

;;; Put in the generic Emacs paths first.  Follow with the local stuff so the
;;; local versions take precedence over the distributes versions.
(let ((directories (if unix-p
                       '("~/EMACS")
                     ;;ELSE
                     (list (concat emacs-home "/lisp")
                           (let ((one "%s/cygwin/home/twekberg/EMACS")
                                 (other "%s/cygwin/home/Tom/EMACS"))
                             (if (file-directory-p (format one emacs-drive))
                                 (format one emacs-drive)
                               ;;else
                               (format other emacs-drive))))))
      directory)
  (while directories
    (setq directory (car directories))
    (if (file-directory-p (expand-file-name directory))
	(add-hook 'load-path (expand-file-name directory)))
    (setq directories (cdr directories))))

(if (not unix-p)
    (setq doc-directory (concat emacs-home "/etc/")))

;;; Enable the upcase-region function.
(put 'upcase-region 'disabled nil)

;;; Delete excess backup versions silently.
(setq delete-old-versions t)

(setq stack-trace-on-error nil)

(defun ignore-key ()
  "Interactive version of ignore" ; taken from emacs/lisp/term/sun.el
  (interactive)
  (ignore))

;;; Simple function to make using the compile command easier.
;;; This one asks no questions.
(defun recompile ()
  (interactive)
  (let ((did-compile nil))
  (if (equal (file-name-extension (buffer-file-name)) "java")
      ;; run-ant will return non-nil when it finds a build.xml file.
      (setq did-compile (run-ant)))
  (if (not did-compile)
      (progn
        (if (not (boundp 'compile-command))
            (set 'compile-command "make"))
        (save-some-buffers t)
        (compile (eval 'compile-command))))))

(defun run-ant ()
  "Run an ant script from the directory where the build.xml file is located.
Search up from this directory to locate the build.xml file. Then temporarily
change default-directory to point there, run the ant command, then change
default-directory back."
  (interactive)
  (let ((here default-directory)
        (keep-looking t)
        (found nil))
    (while (and keep-looking (equal (substring here -1) "/"))
      (if (file-exists-p (concat here "build.xml"))
          (setq keep-looking nil
                found t)
        ;;ELSE
        (setq here (file-name-directory (substring here 0 -1)))
        (if (and (>= (length here) 3)
                 (equal (substring here -2 -1) ":"))
            (setq keep-looking nil))))
    (if found
        (let ((default-directory here))
          (compile (substitute-in-file-name "$ANT_HOME/bin/ant -emacs"))))))

;;; Catch an accidental exit.
(defun really-exit ()
  "Asks if you really want to exit emacs.
If running as emacsclient where emacs was started with --daemon then
this exits the terminal (save-buffers-kill-terminal)."
  (interactive)
  (let ((prog (if (> (length (terminal-list)) 1)
                  "emacsclient"
                "emacs")))
    (if (y-or-n-p (concat "Really exit " prog "? "))
        (progn
          (save-persistent-scratch)
          (if (equal prog "emacs")
              (save-buffers-kill-emacs)
            (save-buffers-kill-terminal))))))

;;; Various key key bindings
(global-set-key "\^X\^A" 'add-global-abbrev)
(global-set-key "\^X\^B" 'buffer-menu)
(global-set-key "\^X\^C" 'really-exit)
(global-set-key "\^X\c"  'recompile)
(global-set-key "\^X\^D" 'dired)
(global-set-key "\^X\G"  'goto-line)
(global-set-key "\^Xg"   'insert-register)
(global-set-key "\^X\^R" 'revert-buffer)
(global-set-key "\^X\t"  'indent-to-edge)
(global-set-key "\^X\^V" 'find-file-other-window)
(global-set-key "\^Xx"   'copy-to-register)
(global-set-key "\^X\^Z" 'really-exit)
(global-set-key "\^L"    'recenter)
(global-set-key "\e$"    'ispell-word)
(global-set-key "\e\^L"  'swap-top-buffers)
(global-set-key "\eM"    'compare-windows)
(global-set-key "\em"    'set-mark-command) ;; For login from home
(global-set-key "\eQ"    'justify-current-line)
(global-set-key "\eq"    'justify-current-line)
(global-set-key "\eS"    'center-line)
(global-set-key "\es"    'center-line)
(global-set-key "\e\t"   'indent-relative)
(global-set-key "\^X\e"  'repeat-complex-command) ; Binding removed in emacs 19
(global-set-key "\^Cd"   'make-new-diary-entry)
(global-set-key "\^Ce"   'end-diary-entry)
(global-set-key "\^Cs"   'simple-spell)

;;; Don't get out on a single keystroke.
(global-set-key "\^Z"     'beep)

;;; Gimme 5 more lines when the cursor goes off the screen
(setq scroll-step 5)

;;; Set up word wrapping to completely fill the line.
(setq fill-column 79)

;;; No noise when starting up.
(setq inhibit-startup-message t)

;;; Enable automatic version control.
(setq version-control t)
(setq trim-versions-without-asking t)	; And don't bother me about it.

;;; In abbreviations mode, all caps abbreviations expand to all caps
;;; in the expansion.
(setq abbrev-all-caps t)

;;; Default to abbreviations mode on.
(setq abbrev-mode t)

(if window-system
    ;; Color stuff.
    (if (> (x-display-color-cells) 2)
	(progn
	  (set-cursor-color "blue")
	  (set-mouse-color "red"))))

;;; Turn on auto fill when composing text or mail.
(add-hook 'text-mode-hook  'setup-text-mode)
(add-hook 'mail-setup-hook 'turn-on-auto-fill)

(defun setup-text-mode ()
  (turn-on-auto-fill)
  (column-number-mode 1)
  (define-key text-mode-map "\e\t" 'indent-relative))


;;; Allow J2EE ear files to be read as zip files.
(add-hook 'auto-mode-alist '("\\.\\(ear\\|EAR\\)\\'" . archive-mode))

;;; Put time and load average in mode line.
(display-time)

;;; The following two functions, twe-remove and twe-substitute, are used to
;;; update the mode line to my taste.  They are similar to the Common Lisp
;;; remove and substitute functions. The "twe-" prefix was added so they don't
;;; conflict with Common Lisp packages.
(defun twe-remove (element list key-function test)
  "Remove the ELEMENTs from LIST if present.
KEY-FUNCTION is used to extract a component from an element of LIST.  If
	KEY-FUNCTION is NIL then this is interpreted to mean that nothing is
	done.
TEST is used to compare the component extracted via KEY-FUNCTION with ELEMENT.
The result returned is LIST with ELEMENTs removed."
  (let ((new-list nil)
	(old-list list))
    ;; Rebuild the LIST into new-list
    (while (not (zerop (length old-list)))
      ;; For each element in old-list, check to see if it matches ELEMENT.
      (if key-function
	  (if (not (funcall test element
			    (funcall key-function (car old-list))))
	      (setq new-list (cons (car old-list) new-list)))
	;;ELSE
	(if (not (funcall test element (car old-list)))
	    (setq new-list (cons (car old-list) new-list))))      
      ;; Go on to the next element of old-list
      (setq old-list (cdr old-list)))
    ;; The earlier cons function made the list in a backward manner.  We need
    ;; to do a reverse to put it in the proper order.
    (reverse new-list)))

(defun twe-substitute (old-element new-element list 
			    key-function test
			    reconstruction-function)
  "Replace the OLD-ELEMENTs from LIST, if present, with NEW-ELEMENTs.
KEY-FUNCTION is used to extract a component from an element of LIST.  If
	KEY-FUNCTION is NIL then this is interpreted to mean that nothing is
	done.
TEST is used to compare the component extracted via KEY-FUNCTION with ELEMENT.
RECONSTRUCTION-FUNCTION is a function of 2 arguments, and element of LIST, and
	the NEW-ELEMENT.  The result of this function is the element of LIST,
	with NEW-ELEMENT substituted at the appropriate place.
The result returned is LIST with OLD-ELEMENTs replaced by NEW-ELEMENTs."
  (let ((new-list nil)
	(old-list list)
	component)
    ;; Rebuild the LIST into new-list
    (while (not (zerop (length old-list)))
      ;; For each element in old-list, check to see if it matches OLD-ELEMENT.
      (setq component (car old-list))
      (if key-function
	  (setq new-list (cons (if (funcall test old-element
					    (funcall key-function
						     component))
				   (funcall reconstruction-function component
					    new-element)
				   ;;ELSE
				   component)
			       new-list))
	;;ELSE
	  (setq new-list (cons (if (funcall test old-element component)
				   (funcall reconstruction-function component
					    new-element)
				   ;;ELSE
				   component)
			       new-list)))
      ;; Go on to the next element of old-list
      (setq old-list (cdr old-list)))
    ;; The earlier cons function made the list in a backward manner.  We need
    ;; to do a reverse to put it in the proper order.
    (reverse new-list)))

(defun cadr (list) (car (cdr list)))
(defun update-cadr (component new-cadr) (list (car component) new-cadr))
(setq minor-mode-alist (twe-substitute " Abbrev" " Abb" minor-mode-alist 'cadr 
				   'equal 'update-cadr))
(setq minor-mode-alist (twe-substitute " Fill" " Fl" minor-mode-alist 'cadr 
				   'equal 'update-cadr))

;;; This code makes the *scratch* buffer persistent.
(load "persistent-scratch" t nil)
(load-persistent-scratch)

(load "align.elc" nil t)

;;;----------------------------------------------------------------------------
;;; C mode stuff.
(load "cc-mode" t nil)

;;; C++ mode stuff.
(autoload 'c++-mode "cc-mode" "C++ Editing Mode" t)
(autoload 'c-mode   "cc-mode" "C Editing Mode" t)

;;; Force C and C++ code to GNU indenting style.
;;(add-hook 'c-mode-hook   'set-gnu-c-style)
;;(add-hook 'c++-mode-hook 'set-gnu-c-style)
;; Commented out the next 3 lines when Dennis' code was added.
;;(defun set-gnu-c-style ()
;;  (c-set-style "GNU"))
;;(setq c-basic-offset 2)

;;; Assume that the .[CH], .hh and .cc files are C++.
(add-hook 'auto-mode-alist '("\\.C$"  . c++-mode))
(add-hook 'auto-mode-alist '("\\.cc$" . c++-mode))
(add-hook 'auto-mode-alist '("\\.H$"  . c++-mode))
(add-hook 'auto-mode-alist '("\\.hh$" . c++-mode))
(add-hook 'auto-mode-alist '("\\.c$"  . c-mode))
(add-hook 'auto-mode-alist '("\\.h$"  . c-mode))

;;; Fixes to turn dash (-) into either underscore or leave it alone.
(load "c-electric-dash.elc" nil t)

;;; Hack to drive */ to the right margin when not preceeded by space
(load "c-electric-comment.elc" nil t)

;;;(load "c-auto-tag-load.elc" nil t)

;;; Python mode stuff.
(autoload 'python-mode   "python-mode" "Python Editing Mode" t)
(add-hook 'auto-mode-alist '("\\.wsgi$"  . python-mode))

;;;------------------------------------------------------------------------------
;;; Code from Dennis Dupont.
;;(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq-default tab-stop-list	'(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80 88 96 104 112 120))

;;; Force C and C++ code to SSA indenting style.
(defconst c-style-ssa
  '((c-basic-offset . 4)
    (c-comment-only-line-offset . 0)
    (c-offsets-alist . 
		     ((inline-open . 0)
		      (statement-block-intro . +)
		      (substatement-open . 0)
		      (label . 0)
		      (statement-cont . 12)
		      (case-label . 0)
		      (comment-intro . 0)
		      (arglist-intro . 12)
		      (arglist-cont-nonempty . 12)
		      (func-decl-cont . 12)))))


(defun FontSetup()
  (if (not (assq 'java-mode font-lock-defaults-alist))
      (setq font-lock-defaults-alist
	    (cons
	     (cons 'java-mode
		   ;; jde-mode-defaults
		   '((java-font-lock-keywords java-font-lock-keywords-1
					      java-font-lock-keywords-2
					      java-font-lock-keywords-3)
		     nil nil ((?_ . "w") (?$ . "w")) nil
		     (font-lock-mark-block-function . mark-defun)))

	     font-lock-defaults-alist)))
  (setq font-lock-maximum-decoration t)
  (global-font-lock-mode 1))


(add-hook 'c-mode-hook   'set-ssa-c-style)
(add-hook 'c++-mode-hook 'set-ssa-c-style)
(add-hook 'java-mode-hook 'set-ssa-c-style)
(add-hook 'jde-mode-hook 'set-ssa-c-style)
(defun set-ssa-c-style ()
  (interactive)
  ;;(FontSetup)
  (column-number-mode 1)
  (setq-default c-indent-comments-syntactically-p t)
;  (define-key c-mode-map "\t"        'tab-to-tab-stop)
  (define-key c-mode-map "\C-m"      'PrettyNewline)
;  (define-key c++-mode-map "\t"      'tab-to-tab-stop)
  (define-key c++-mode-map "\C-m"    'PrettyNewline)
;  (define-key java-mode-map "\t"     'tab-to-tab-stop)
  (define-key java-mode-map "\C-m"   'PrettyNewline)
  (c-add-style "ssa" c-style-ssa t))

(defun PrettyRegion ()
  "Format region with correct indentation"
  (interactive)
      (c-indent-region (region-beginning) (region-end))
)

(defun PrettyNewline ()
  "Insert a newline with correct indentation"
  (interactive)
      (newline)
      (c-indent-command)
)

;;; End of Code from Dennis Dupont.
;;;----------------------------------------------------------------------------

;;; Indents so that the rightmost character is at the fill column.
;;; Tied to C-x TAB.
(load "indent-to-edge.elc" nil t)

(load "insert-header.elc" nil t)

;;; reStructuredText markup language.
(require 'rst)

(load "insert-javadoc.elc" nil t)
;;; Bind the JavaDoc generator to Meta-J.
(global-set-key "\eJ" 'insert-javadoc)

;;; Load in the install-macro command.
(load "install-macro.elc" nil t)

;;; Insert HTML code into a page that indicates when the page was modified
;;; last.
(load "insert-modified-date.elc" nil t)

;;; Load in my general describe function.
(load "describe.elc" nil t)

(load "desktop" nil t)
;; should save desktop periodically instead of just on exit, but not
;; if emacs is started with --no-desktop
(if desktop-save-mode
    (progn
      (message "Enabling desktop auto-save")
      (add-hook 'auto-save-hook 'desktop-save-in-desktop-dir)))

(desktop-save-mode 1)

;;; Override the one in desktop.el
(defun desktop-save-in-desktop-dir (&optional noisy)
  "Save the desktop in directory `desktop-dirname'."
  (interactive)
  (if desktop-dirname
      (desktop-save desktop-dirname)
    (call-interactively 'desktop-save))
  (if noisy
      (message "Desktop saved in %s" (abbreviate-file-name desktop-dirname))))

;;; The following form removes the "%n" string from the mode-line-format.
;;; The %n string puts a "Narrow" string in the mode line for rmail messages
;;; which makes the mode line too long to show much of anything else.
(setq mode-line-format (twe-remove "%n" mode-line-format nil 'equal))
(setq mode-line-format mode-line-format)

;;; Define L to load in an elisp file.
(load "dired" t nil)
(define-key dired-mode-map "L" 'dired-load-elisp)

(defun dired-load-elisp ()
  "Load this elisp file."
  (interactive)
  (let* ((elisp-file (dired-get-filename)))
    (if (string-match "\\.elc?$" elisp-file) nil
	(error "%s is not an elisp file!" elisp-file))
    (load-file elisp-file)))

;;; Redefine the dired predicate for backup files.
(defvar dired-backup-file-patterns '("~$" "^#.*#$" "\\.backup$" "^core$"
				     "^.nfs" "\\.\\.c$")
  "List of pattern strings to match backup files.")

;;; If non-nil, list of symbols for commands dired should not confirm.  It can
;;; be a sublist of:
;;;  '(byte-compile chgrp chmod chown compress copy delete hardlink load
;;;    move print shell symlink uncompress)"
(setq dired-no-confirm '(byte-compile compress load print uncompress))


;;; From 19.18 dired.el.  Removed first and clause that does preliminary
;;; checking for the backup file.  This version uses backup-file-name-p to
;;; determine whether or not this is a backup file.
(defun dired-flag-backup-files (&optional unflag-p)
  "Flag all backup files (names ending with `~') for deletion.
With prefix argument, unflag these files."
  (interactive "P")
  (let ((dired-marker-char (if unflag-p ?\040 dired-del-marker)))
    (dired-mark-if
     ;; It is less than general to check for ~ here,
     ;; but it's the only way this runs fast enough.
     (and (not (looking-at dired-re-dir))
	  (let ((fn (dired-get-filename t t)))
	    (if fn (backup-file-name-p fn))))
     "backup file")))

(defun backup-file-name-p (file)
  "Return non-nil if FILE is a backup file name (numeric or not).
This is a separate function so you can redefine it for customization.
You may need to redefine file-name-sans-versions as well."
  (let (a-pattern
	(patterns dired-backup-file-patterns)
	(more-remaining t))
  (while (and (setq a-pattern (car patterns))
	      more-remaining)
    (if (string-match a-pattern file)
	(setq more-remaining nil))
    (setq patterns (cdr patterns)))
  (if more-remaining
      nil
    t)))

;;; Load in the eval-into-buffer command.
(load "eval-into-buffer.elc" nil t)

;;; Return a list of all windows, in from top-to-bottom order.
;;; Note: Ordering of horizontal windows is unspecified.
(defun list-all-windows ()
  (let (a-window a
        (window-list (list (list (cadr (window-edges (selected-window))) (selected-window))))
	(first-window (selected-window))
	(last-window (selected-window)))
    ;; 0 says to not include the minibuffer even if it is active.
    ;; 1 says to only look at this frame
    (while (not (eq (setq a-window (next-window last-window 0 1)) first-window))
      (setq window-list (cons (list (cadr (window-edges a-window)) a-window) window-list))
      (setq a window-list)
      (setq last-window a-window))
    ;; Put the windows in a top-to-bottom order.
    (mapcar 'cadr (sort window-list (lambda (x y) (< (car x) (car y)))))))

;;; If there are exactly 2 windows, run ediff-buffers on them.
(defun ediff-window ()
  (interactive)
  (let ((windows (list-all-windows)))
    (if (= (length windows) 2)
	(apply 'ediff-buffers (mapcar 'window-buffer windows))
      ;;ELSE
      (ediff-buffers))))
(setq ediff-temp-file-prefix (if unix-p "/tmp/"          (format "%s/temp/"                emacs-drive)))
(setq ediff-diff-program     (if unix-p "/usr/bin/diff"  (format "%s/cygwin/bin/diff.exe"  emacs-drive)))
(setq ediff-diff3-program    (if unix-p "/usr/bin/diff3" (format "%s/cygwin/bin/diff3.exe" emacs-drive)))


;;; Load in the recenter fix for GDB.
;;;(load "gdb-fix.elc" nil t)

(load "tempo.elc" nil t)
(load "font-lock" nil t)
(autoload 'html-helper-mode "html-helper-mode" "Yay HTML" t)
(add-hook 'auto-mode-alist '("\\.html$" . html-helper-mode))
(add-hook 'auto-mode-alist '("\\.jsp$" . html-helper-mode))
;;; Prompt user for strings in templates.
(setq tempo-interactive t)

;;; Get ready to load ksh mode.
;;;(autoload 'ksh-mode "ksh-mode" "Major mode for editing sh Scripts." t)
;;;(setq ksh-indent 2)

;;; Use the one that comes with Emacs.  It looks much better.
(autoload 'sh-mode "sh-script" "Major mode for editing shell scripts." t)
(setq sh-tab-width 2)

;;; Scripts to be automatically run by shells usually lack extensions;
;;; the following code parses the #! magic at the start of the script
;;; instead.  As written, this overrides auto-mode-alist; that could
;;; be changed by having parse-script-magic check for Fundamental mode
;;; first.

;;; Removed the following since I got stuck trying to load my .cshrc file
;;;      ("^#!.*[kb]?sh"	. sh-mode)

(defvar script-magic-alist '(("^#!.*perl"	. perl-mode)
			     ("^#!.*wish"	. tcl-mode)
			     ("^#!.*tcl"	. tcl-mode)))

(defun parse-script-magic ()
  (let ((mode (save-excursion
		(goto-char (point-min))
		(let ((codes script-magic-alist))
		  (catch 'found
		    (while codes
		      (let ((key  (car (car codes)))
			    (mode (cdr (car codes))))
			(if (looking-at key)
			    (throw 'found mode)))
		      (setq codes (cdr codes)))
		    nil)))))
    (if mode (funcall mode))))

(add-hook 'find-file-hooks 'parse-script-magic)


(autoload 'perl-mode "cperl-mode" "alternate mode for editing Perl programs" t)
(setq cperl-hairy nil)
(setq cperl-indent-level 4)
;; Turn off the code that reformats (all I want is the indentation fixed).
(setq cperl-indent-region-fix-constructs nil)
(setq auto-mode-alist
      (append '(("\\.\\([pP][Llm]\\|al\\)$" . perl-mode))  auto-mode-alist ))

(load "insert-change-history.elc" nil t)
;; Bind it to Meta-Shift-;
(global-set-key "\e:" 'insert-change-history)

;;; For some reason lisp mode has a comment end of star slash, which causes
;;; trouble for insert-change-history.
(defun fix-lisp-mode-comments ()
  (setq comment-end ""))

(add-hook 'emacs-lisp-mode-hook       'fix-lisp-mode-comments)
(add-hook 'lisp-mode-hook             'fix-lisp-mode-comments)
(add-hook 'lisp-interaction-mode-hook 'fix-lisp-mode-comments)

;;; Set up the WCL editing hacks.
;;;(load "wcl/man.elc" nil t)
;;;(load "wcl/xmr-database.elc" nil t)
;;;(load "wcl/xmr-mode.elc" nil t)
;;;(add-hook 'auto-mode-alist '("\\-resource$" . xmr-mode))

(load "man" nil t)

;;; Modification to support using the mouse in man pages.
(add-hook 'Man-mode-hook
	  '(lambda ()
	     (Man-mouseify-xrefs)
	     (define-key Man-mode-map "\r" 'Man-do-manual-reference)
	     (define-key Man-mode-map "\t" 'Man-next-manual-reference)
	     (define-key Man-mode-map "\e\t" 'Man-prev-manual-reference)
	     (define-key Man-mode-map [mouse-2] 'Man-mouse-manual-reference)
	     ))
(autoload 'Man-mouseify-xrefs "man-xref")

(defvar symbol-mode nil
  "Special mode for editing NMS Simulator symbol table files.")

(defun symbol-mode ()
  (interactive)
  (setq symbol-mode (not symbol-mode))
  (text-mode)
  (setq comment-start-skip "#+ *")
  (setq comment-start "#"))

(add-hook 'auto-mode-alist '("\\-symbols.text$" . symbol-mode))

;;; Set up for CaseWare PTS files.
(add-hook 'auto-mode-alist '("_edit[1-9][0-9]*$"        . text-mode))
(add-hook 'auto-mode-alist '("_comments[1-9][0-9]*$"    . text-mode))
(add-hook 'auto-mode-alist '("sb_prob_des[1-9][0-9]*$"  . text-mode))
(add-hook 'auto-mode-alist '("Amplify[a-z][0-9][0-9]*$" . text-mode))

;;; Need a different backup file for CaseWare.  Checks to see if we can write
;;; the auto save file in the directory.  If not, write it to pwa.
(defvar auto-save-default-directory "~/pwa/"
  "Place to write auto-save files if the preferred directory is not writable.")

(add-hook 'auto-mode-alist '("\\.pas$" . text-mode))

(defun make-auto-save-file-name ()
  "Return file name to use for auto-saves of current buffer.  (TWE)
Does not consider `auto-save-visited-file-name' as that variable is checked
before calling this function.  You can redefine this for customization.
See also `auto-save-file-name-p'."
  (let ((auto-file (if buffer-file-name
		       (concat (file-name-directory buffer-file-name)
			       "#"
			       (file-name-nondirectory buffer-file-name)
			       "#")
		     ;; For non-file bfr, use bfr name and Emacs pid.
		     (expand-file-name (format "#%s#%s#" (buffer-name)
					       (make-temp-name ""))))))
    (if (and (not (file-writable-p auto-file))
	     (file-writable-p auto-save-default-directory))
	(if buffer-file-name
	    (concat (expand-file-name auto-save-default-directory)
		    "#"
		    (file-name-nondirectory buffer-file-name)
		    "#")
	  ;; For non-file buffer, use buffer name and Emacs pid.
	  (expand-file-name (format "#%s#%s#" (buffer-name)
					       (make-temp-name ""))))
      auto-file)))

(add-hook 'comint-mode-hook 'setup-prompt-regexp)

(defun setup-prompt-regexp ()
  "Change the prompt in the shell buffer to match my prompt."
  (setq comint-prompt-regexp "([A-Za-z0-9]*) [1-9][0-9]*\. "))

;;; Get rid of the annoying scroll inside of a shell window.
(setq comint-scroll-show-maximum-output nil)

;;; Load in the mouse cut/paste code.
;;;(load "emacs-cut-paste.elc" nil t)

;;; Load in the shell indent region code.
(load "shell-indent-region.elc" nil t)

;;; Load the Emacs server code to allow connections from the emacsclient
;;; program.
(load "server")

;;; Define a simple function to help with daily diary entries.
(load "time-stamp")
(defun make-new-diary-entry ()
  "Prepares for entry of a new diary entry."
  (interactive)
  (goto-char (point-max))
  (insert
   (if (bolp) "" "\n\n")
   "-------------------------------------------------------------------------------\n"
   (time-stamp-string "%:y-%02m-%02d %02H:%02M:%02S %:a %u") " "
   "\n\n"))

(defun end-diary-entry ()
  "Annotation for the end of a diary entry."
  (interactive)
  (goto-char (point-max))
  (insert
   (if (bolp) "\n" "\n\n")
   "Ended "
   (time-stamp-string)))

;;; Prepare emacs for use as a server.
(if unix-p
    (server-start))

;;; 160 is too narrow.
(setq split-width-threshold 200)

;;; Local Variables:
;;; eval: (defun byte-compile-this-file () (write-region (point-min) (point-max) buffer-file-name nil 't) (byte-compile-file buffer-file-name) nil)
;;; write-file-hooks: (byte-compile-this-file)
;;; End:
